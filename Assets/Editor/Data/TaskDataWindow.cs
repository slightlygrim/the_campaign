﻿using System.Collections.Generic;
using System.IO;
using Data;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class TaskDataWindow : WindowData
    {
        private static List<TasksData> _allTasksData = new List<TasksData>();
        private static TasksData _currentTasks;
        private static TaskData _currentTask;

        private static int loaded;
        private static Vector2 _scrollPos = Vector2.zero;
        private static string _path;

        [MenuItem("Campaign/Task Data")]
        private static void OpenWindow()
        {
            var window = (TaskDataWindow) GetWindow(typeof(TaskDataWindow));
            window.minSize = new Vector2(600,300);
            var content = new GUIContent {text = "Task Data"};
            window.titleContent = content;
            window.Show();
        }

        private void OnEnable()
        {
            InitData();
            LoadAllTasks();
        }

        private void OnGUI()
        {
            if (loaded == 0) DrawTasksSection();
            else if (loaded == 1) DrawTaskList(_currentTasks);
            else if (loaded == 2) DrawTask(_currentTask);
        }
    
        private static void InitData()
        {
            _path = Application.persistentDataPath + "/Tasks/TasksData.json";
        }

        private void DrawTasksSection()
        {
            EditorGUILayout.BeginVertical();
            _scrollPos = GUILayout.BeginScrollView(_scrollPos,false,true);
            {
                var value = 0;
                
                foreach (var tasks in _allTasksData)
                {
                    if (GUILayout.Button(tasks.title, GUILayout.Height(40)))
                    {
                        loaded = 1;
                        _currentTasks = tasks;
                    }
                }
                
                GUILayout.Space(20);
                if (GUILayout.Button("Add", GUILayout.Height(40)))
                {
                    var newTasks = new TasksData();
                   _allTasksData.Add(newTasks);
                   _currentTasks = newTasks;
                   loaded = 1;
                }
                GUILayout.Space(20);
                
                if (GUILayout.Button("Save All", GUILayout.Height(40)))
                {
                   
                }
            }
            EditorGUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        private void DrawTaskList(TasksData tasks)
        {
            EditorGUILayout.BeginVertical();
            //var style = new GUIStyle(GUI.skin.button) {normal = {textColor = Color.white}};

            if (_currentTasks.tasks.Count == 0)
            {
                if (GUILayout.Button("add", GUILayout.Height(20)))
                {
                    _currentTask = new TaskData();
                    loaded = 2;
                }
            }
            else
            {
                foreach (var task in _currentTasks.tasks)
                {
                    if (GUILayout.Button(task.title, GUILayout.Height(20)))
                    {
                        _currentTask = task;
                        loaded = 2;
                    }
                }
            }
            
            if (GUILayout.Button("back", GUILayout.Height(20)))
            {
                loaded = 1;
            }
            
            EditorGUILayout.EndVertical();
        }
        
        private void DrawTask(TaskData task)
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();
            var defaultColor= GUI.backgroundColor;
            GUI.backgroundColor= Color.gray;
            
            _currentTask.title = TextField("Name:", _currentTask.title);
            _currentTask.fundingValue = FloatField("Funding", _currentTask.fundingValue);
            _currentTask.timeValue = FloatField("Time", _currentTask.timeValue);
            _currentTask.votesValue = FloatField("Votes", _currentTask.votesValue);
            
            EditorGUILayout.EndVertical();
            
            task.taskImage = Thumbnail(task.taskImage);
            
            
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();

            var style = new GUIStyle(GUI.skin.button) {normal = {textColor = Color.white}};

            if (GUILayout.Button("Save", style, GUILayout.Height(20)))
            {
                if (_currentTasks.tasks.Contains(_currentTask))
                {
                    for (var i = 0; i < _currentTasks.tasks.Count; i++)
                    {
                        if (_currentTasks.tasks[i] == _currentTask)
                        {
                            _currentTasks.tasks[i] = _currentTask;
                        }
                    }
                }
                else
                {
                    _currentTasks.tasks.Add(_currentTask);
                }
                loaded = 1;
            }
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Remove", style, GUILayout.Height(20)))
            {
                if (_currentTasks.tasks.Contains(_currentTask))
                {
                    _currentTasks.tasks.Remove(_currentTask);
                }

                loaded = 1;
            }
            
            GUI.backgroundColor=defaultColor;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }
        
        private static void SaveTasks(EmployeeData employeeData, int number)
        {
            var list = new TasksData();
            
            if (!Directory.Exists(Application.persistentDataPath + "/Tasks"))
            {
                Directory.CreateDirectory(Application.persistentDataPath + "/Tasks");
            }
            
            if (File.Exists(_path))
            {
                var text = File.ReadAllText(_path);
                list = JsonUtility.FromJson<TasksData>(text);
                var jsonData = JsonUtility.ToJson(list);
                File.WriteAllText(_path,jsonData);
            }
            else
            {
                var textData = JsonUtility.ToJson(employeeData);
                File.WriteAllText(_path, textData);
                loaded = 0;
            }
        }
        
        private static void LoadAllTasks()
        {
            var task = SaveData.LoadAllTasksData();
            if (task == null)
            {
                _allTasksData.Add(new TasksData());
            }
            else
            {
                _allTasksData = task;
            }
        }
        
    }
}
