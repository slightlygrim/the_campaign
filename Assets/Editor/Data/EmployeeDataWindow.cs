﻿using System.IO;
using Data;
using Types;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Editor
{
    public class EmployeeDataWindow : WindowData
    {
        private static EmployeesData _employeesData;
        private static EmployeeData _currentEmployee;
        private static int _employeeListPosition;

        private static bool loaded;
        private static Vector2 _scrollPos = Vector2.zero;
        private static string _path;

        [MenuItem("Campaign/Employee Data")]
        private static void OpenWindow()
        {
            var window = (EmployeeDataWindow) GetWindow(typeof(EmployeeDataWindow));
            window.minSize = new Vector2(600,300);
            var content = new GUIContent {text = "Employee Data"};
            window.titleContent = content;
            window.Show();
        }

        private void OnEnable()
        {
            InitData();
            LoadEmployees();
        }

        private void OnGUI()
        {
            if (loaded)
            {
                DrawEmployee(_currentEmployee);
            }
            else if(!loaded)
            {
                DrawEmployees();
            }
        }
    
        private static void InitData()
        {
            _path = Application.persistentDataPath + "/Employees/EmployeesData.json";
        }

        private void DrawEmployees()
        {
            EditorGUILayout.BeginVertical();
            _scrollPos = GUILayout.BeginScrollView(_scrollPos,false,true);
            {
                var value = 0;

                if (_employeesData.employees.Count != 0)
                {
                    foreach (var employee in _employeesData.employees)
                    {
                        if (employee.Name == null)
                        {
                            employee.Name = "new";
                        }
                        
                        if (GUILayout.Button(employee.Name, GUILayout.Height(40)))
                        {
                            loaded = true;
                            _currentEmployee = employee;
                            _employeeListPosition = value;
                        }

                        value++;
                    }
                }
                
                GUILayout.Space(20);
                if (GUILayout.Button("Add", GUILayout.Height(40)))
                {
                    var employee = new EmployeeData();
                    _employeesData.employees.Add(employee);
                    employee.Name = "new";
                }
                GUILayout.Space(20);
                
                if (GUILayout.Button("Save All", GUILayout.Height(40)))
                {
                    SaveAll(_path, _employeesData);
                }
            }
            EditorGUILayout.EndScrollView();
            GUILayout.EndVertical();
        }

        private void DrawEmployee(EmployeeData employee)
        {
            EditorGUILayout.BeginVertical();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Title:", GUILayout.Width(100));
            employee.title = (TitleType) EditorGUILayout.EnumPopup(employee.title);
            EditorGUILayout.EndHorizontal();
            
            employee.Name = TextField("Name:", employee.Name);
            employee.jobTitle = TextField("Position:", employee.jobTitle);
            employee.numberSkills = IntField("MaxSkills:", employee.numberSkills);
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Type of Skills:", GUILayout.Width(100));
            employee.TypeSkills = (EmployeeTypeSkills) EditorGUILayout.EnumPopup(employee.TypeSkills);
            EditorGUILayout.EndHorizontal();
            

            EditorGUILayout.BeginHorizontal();
            var defaultColor= GUI.backgroundColor;
            GUI.backgroundColor= Color.gray;

            var style = new GUIStyle(GUI.skin.button) {normal = {textColor = Color.white}};

            if (GUILayout.Button("Save", style, GUILayout.Height(20)))
            {
                SaveEmployee(employee, _employeeListPosition);
                loaded = false;
            }
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Remove", style, GUILayout.Height(20)))
            {
                _employeesData.employees.Remove(employee);
            }
            GUI.backgroundColor=defaultColor;
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();
        }
        
        private static void SaveEmployee(EmployeeData employeeData, int number)
        {
            var list = new EmployeesData();
            
            if (!Directory.Exists(Application.persistentDataPath + "/Employees"))
            {
                Directory.CreateDirectory(Application.persistentDataPath + "/Employees");
            }
            
            if (File.Exists(_path))
            {
                var text = File.ReadAllText(_path);
                list = JsonUtility.FromJson<EmployeesData>(text);
                if (list.employees.Count <= number)
                {
                    list.employees.Add(employeeData);
                }
                else
                {
                    list.employees[number] = employeeData;
                }
                var jsonData = JsonUtility.ToJson(list);
                File.WriteAllText(_path,jsonData);
            }
            else
            {
                var textData = JsonUtility.ToJson(employeeData);
                File.WriteAllText(_path, textData);
                loaded = false;
            }
        }

        private static void LoadEmployees()
        {
            _employeesData = !File.Exists(_path) ? new EmployeesData() : SaveData.LoadAllEmployeesData();
        }
        
    }
}
