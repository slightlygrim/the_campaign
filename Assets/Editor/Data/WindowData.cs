﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class WindowData : EditorWindow
    {
        protected string TextField(string title, string value)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( title, GUILayout.Width(100));
            value = EditorGUILayout.TextField(value);
            EditorGUILayout.EndHorizontal();
            return value;
        }

        protected Texture2D Thumbnail(Texture2D value)
        {
            EditorGUILayout.BeginVertical();
            value = (Texture2D)EditorGUILayout.ObjectField(value, typeof(Texture2D), GUILayout.Width(100), GUILayout.Height(100));
            EditorGUILayout.EndVertical();
            return value;
        }

        protected float FloatField(string title, float value)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( title, GUILayout.Width(100));
            value = EditorGUILayout.FloatField(value);
            EditorGUILayout.EndHorizontal();
            return value;
        }

        protected int IntField(string title, int value)
        {
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label( title, GUILayout.Width(100));
            value = EditorGUILayout.IntField(value);
            EditorGUILayout.EndHorizontal();
            return value;   
        }
        
        protected void SaveAll<T>(string path, T param)
        {
            var textData = JsonUtility.ToJson(param);
            File.WriteAllText(path, textData);
        }
        
        protected object LoadData<T>(string path)
        {
            if (!File.Exists(path)) return null;
            var data = File.ReadAllText(path);
            return JsonUtility.FromJson<T>(data);
        }
    }
}
