﻿using System.IO;
using Data;
using Types;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class CandidateDataWindow: WindowData
    {
        private Texture2D _headerSectionTexture;
        private Rect _headerSection;
        private Rect _candidateSection;
        private static CandidateData CandidateData { get; set; }
        private static CandidatesData candidates;
        
        private static string Path;
        
        private Vector2 _scrollPos = Vector2.zero;
        
        [MenuItem("Campaign/Candidate Data")]
        private static void OpenWindow()
        {
            var window = (CandidateDataWindow) GetWindow(typeof(CandidateDataWindow));
            window.minSize = new Vector2(600,300);
            window.Show();
        }
        
        private void OnEnable()
        {
            InitData();
            LoadCandidates();
        }

        private static void InitData()
        {
            CandidateData = new CandidateData();
            Path = Application.persistentDataPath + "/Candidates/CandidateData.json";
        }

        private void OnGUI()
        {
            DrawCandidateDataLayout();
        }

        private void DrawCandidateDataLayout()
        {
            if (candidates.candidates.Count == 0) return;
            EditorGUILayout.BeginVertical();
            _scrollPos = GUILayout.BeginScrollView(_scrollPos,false,true);
            {
                var value = 0;
                
                foreach (var candidate in candidates.candidates)
                {
                    DrawCandidateSettings(value);
                    value++;
                }
                
                GUILayout.Space(20);
                if (GUILayout.Button("Add", GUILayout.Height(40)))
                {
                    var emptyCandidate = new CandidateData();
                    DrawCandidateSettings(candidates.candidates.Count+1);
                    DrawCandidateDataLayout();
                }
                GUILayout.Space(20);
                
                if (GUILayout.Button("Save All", GUILayout.Height(40)))
                {
                    SaveAll();
                }
            }
            EditorGUILayout.EndScrollView();
            GUILayout.EndVertical();
        }
        
        private void DrawCandidateSettings(int number)
        {
            var candidate = new CandidateData();
            
            if (number < candidates.candidates.Count)
            {
                candidate = candidates.candidates[number];
            }
            else
            {
                candidates.candidates.Add(candidate);
            }

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginVertical();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Title:", GUILayout.Width(50));
            candidate.Title = (TitleType) EditorGUILayout.EnumPopup(candidate.Title);
            EditorGUILayout.EndHorizontal();
            candidate.Name = TextField("Name:", candidate.Name);
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("District:", GUILayout.Width(100));
            candidate.District = (DistrictType) EditorGUILayout.EnumPopup(candidate.District);
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Party:", GUILayout.Width(100));
            candidate.Party = (PartyType) EditorGUILayout.EnumPopup(candidate.Party);
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();

            candidate.thumbnail = Thumbnail(candidate.thumbnail);
            
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.BeginHorizontal();
            Color defaultColor= GUI.backgroundColor;
            GUI.backgroundColor= Color.gray;
            
            var style = new GUIStyle(GUI.skin.button);
            style.normal.textColor = Color.white;

            if (GUILayout.Button("Save", style, GUILayout.Height(20)))
            {
                SaveCandidate(candidate, number);
            }
            
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Remove", style, GUILayout.Height(20)))
            {
                RemoveCandidate(candidate);
            }
            GUI.backgroundColor=defaultColor;
            EditorGUILayout.EndHorizontal();
            
            EditorGUILayout.Space();
        }
        
        private static void SaveCandidate(CandidateData candidateData, int number)
        {
            var list = new CandidatesData();

            if (!Directory.Exists(Application.persistentDataPath + "/Candidates"))
            {
                Directory.CreateDirectory(Application.persistentDataPath + "/Candidates");
            }
            
            if (File.Exists(Path))
            {
                var text = File.ReadAllText(Path);
                list = JsonUtility.FromJson<CandidatesData>(text);
                list.candidates[number] = candidateData;
                var jsonData = JsonUtility.ToJson(list);
                File.WriteAllText(Path,jsonData);
            }
            else
            {
                var textData = JsonUtility.ToJson(candidates);
                File.WriteAllText(Path, textData);
            }
        }

        private static void SaveAll()
        {
            var textData = JsonUtility.ToJson(candidates);
            File.WriteAllText(Path, textData);
        }

        private void LoadCandidates()
        {
            if (!File.Exists(Path)) return;
            var data = File.ReadAllText(Path);
            candidates = JsonUtility.FromJson<CandidatesData>(data);
        }

        private void RemoveCandidate(CandidateData candidateData)
        {
            candidates.candidates.Remove(candidateData);
        }
    }
}
