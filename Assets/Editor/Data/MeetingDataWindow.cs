﻿using System.IO;
using System.Linq;
using Data;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    public class MeetingDataWindow: WindowData
    {
        private Rect _MeetingSection;
        private static Meetings Meetings { get; set; }
        private static Meeting currentMeeting { get; set; }
        private static string[] meetingsTitle;
        
        private static string Path;
        private static int index;
        private static bool loaded = false;
        private Vector2 _scrollPos = Vector2.zero;
        
        [MenuItem("Campaign/Meeting Data")]
        private static void OpenWindow()
        {
            var window = (MeetingDataWindow) GetWindow(typeof(MeetingDataWindow));
            window.minSize = new Vector2(600,300);
            var content = new GUIContent();
            content.text = "Meeting Data";
            window.titleContent = content;
            window.Show();
        }
        
        private void OnEnable()
        {
            Meetings = SaveData.LoadMeetings();
            InitData();
        }

        private static void InitData()
        {
            Path = Application.persistentDataPath + "/Events/Meetings.json";
            SetupTitles();
        }
        
        private void OnGUI()
        {
            if (!loaded)
            {
                DrawSelectMeetingsLayout();
            }
            else if (loaded)
            {
                _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos, false, true);
                {
                    EditorGUILayout.BeginVertical();
                    currentMeeting.title = TextField("Meeting Title", currentMeeting.title);
                    GUILayout.Space(10);
                    EditorGUILayout.EndVertical();
                
                    for (var i = 0; i < currentMeeting.MeetingConversation.Count; i++)
                    {
                        DrawSentences(currentMeeting.MeetingConversation[i], i);
                    }

                    if (GUILayout.Button("Add"))
                    {
                        var emptySentence = new MeetingData();
                        currentMeeting.MeetingConversation.Add(emptySentence);
                    }

                    if (GUILayout.Button("Save Meeting"))
                    {
                        SaveMeeting();
                    }
                    EditorGUILayout.Space();
                
                    if (GUILayout.Button("Unload"))
                    {
                        SetupTitles();
                        loaded = false;
                    }
                };
                EditorGUILayout.EndScrollView();
            }
        }

        private void DrawSelectMeetingsLayout()
        {
            if (Meetings == null)
            {
                Meetings = new Meetings();
                AddMeeting();
                SetupTitles();
            }

            EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Party:", GUILayout.Width(100));
            index = EditorGUILayout.Popup(index, meetingsTitle);
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Load"))
            {
                currentMeeting = Meetings.MeetingsList[index];
                loaded = true;
            }

            if (GUILayout.Button("Add Meeting"))
            {
                AddMeeting();
                loaded = true;
            }
        }

        private void AddMeeting()
        {
            var meeting = new Meeting();
            var meetingData = new MeetingData();
            meeting.title = "empty";
            meeting.MeetingConversation.Add(meetingData);
            Meetings.MeetingsList.Add(meeting);
            currentMeeting = meeting;
        }
        
        private void DrawSentences(MeetingData meetingData, int number)
        {
            EditorGUILayout.BeginVertical();
            meetingData.Sentence = TextField("Sentence", meetingData.Sentence);
            meetingData.Accept = TextField("Accept Title", meetingData.Accept);
            meetingData.AcceptValue = FloatField("Accept Value", meetingData.AcceptValue);
            meetingData.Decline = TextField("Decline Title", meetingData.Decline);
            meetingData.DeclineValue = FloatField("Decline Value", meetingData.DeclineValue);
            EditorGUILayout.EndVertical();
            
            EditorGUILayout.BeginHorizontal();
            Color defaultColor= GUI.backgroundColor;
            GUI.backgroundColor= Color.gray;
            
            var style = new GUIStyle(GUI.skin.button);
            style.normal.textColor = Color.white;

            if (GUILayout.Button("Save Sentence", style, GUILayout.Height(20)))
            {
                SaveSentence(meetingData, number);
            }
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Remove", style, GUILayout.Height(20)))
            {
                RemoveSentence(meetingData);
            }
            
            GUILayout.Space(10);
            
            if (GUILayout.Button("Add", style, GUILayout.Height(20)))
            {
                var emptySentence = new MeetingData();
                currentMeeting.MeetingConversation.Add(emptySentence);
            }
            GUI.backgroundColor=defaultColor;
            EditorGUILayout.EndHorizontal();
        }

        private static void SetupTitles()
        {
            if (Meetings == null) return;
            meetingsTitle = new string[Meetings.MeetingsList.Count];
            for (var i = 0; i < Meetings.MeetingsList.Count; i++)
            {
                meetingsTitle[i] = Meetings.MeetingsList[i].title;
            }
        }
        
        private static void SaveMeeting()
        {
            if (!CheckListMeetings())
            {
                Meetings.MeetingsList.Add(currentMeeting);
            }
            SaveData.SaveMeetings(Meetings);
        }

        private static bool CheckListMeetings()
        {
            foreach (var meeting in Meetings.MeetingsList.Where(meeting => meeting.title == currentMeeting.title))
            {
                meeting.MeetingConversation = currentMeeting.MeetingConversation;
                return true;
            }

            return false;
        }
        
        private static void SaveSentence(MeetingData sentence, int number)
        {
            currentMeeting.MeetingConversation[number] = sentence;
        }

        private void RemoveSentence(MeetingData sentence)
        {
            currentMeeting.MeetingConversation.Remove(sentence);
        }
    }
}
