﻿using System.IO;
using Data;
using UnityEngine;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;

public class CandidateDataEditor : OdinEditorWindow
{
    private static CandidateData candidateData { get; set; }
    private static CandidatesData candidates;
    private static string Path;
    
    [MenuItem("Campaign/CandidateEditor")]
    private static void OpenWindow()
    {
        GetWindow<CandidateDataEditor>().Show();
    }
    
    [TableList(ShowIndexLabels = true)]
    public CandidatesData Candidates = LoadCandidates();
    
    [PropertyOrder]
    [VerticalGroup]
    [Button(ButtonSizes.Large)]
    public void SaveAll() { }
    
    private void Initialise()
    {
        candidateData = new CandidateData();
        candidates = new CandidatesData();
        Path = Application.persistentDataPath + "/Candidates/CandidateData.json";
        LoadCandidates();   
    }

    private static CandidatesData LoadCandidates()
    {
        if (!File.Exists(Path)) return null;
        var data = File.ReadAllText(Application.persistentDataPath + "/Candidates/CandidateData.json");
        var allCandidatesData = JsonUtility.FromJson<CandidatesData>(data);
        return allCandidatesData;
    }
    
}
