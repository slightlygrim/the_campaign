﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

public class ThemeEditor : OdinEditorWindow
{
    [MenuItem("Appreance/Color Theme")]
    private static void OpenWindow()
    {
        GetWindow<ThemeEditor>().Show();
    }

    [ValueDropdown("FetchColorPalettes", IsUniqueList = true, DropdownTitle = "Select Palette", DrawDropdownForListElements = false, ExcludeExistingValuesInList = true)]
    [OnValueChanged("UpdateColors"), Title("CurrentPalette")]
    private string currentPaletteName;
    [ColorPalette, HideLabel]
    public Color color;

    [AssetSelector(DisableListAddButtonBehaviour = true)]
    public Material[] Materials;
    
    private Dictionary<string, Color[]> Palettes = new Dictionary<string, Color[]>();
    private Color[] currentPalette;
    
    private IEnumerable FetchColorPalettes()
    {  
        var palettes = ColorPaletteManager.Instance.ColorPalettes;
        foreach (var palette in palettes)
        {
            if(!Palettes.ContainsKey(palette.Name)) Palettes.Add(palette.Name, palette.Colors.ToArray());
        }
        
        return ColorPaletteManager.Instance.ColorPalettes.Select(x => new ValueDropdownItem(x.Name, x.Colors[0]));
    }

    private void FetchMaterials()
    {
        var 
    }

    private void UpdateColors()
    {
        currentPalette = Palettes[currentPaletteName];
        
        if(currentPalette != null) color = currentPalette[0];

        UpdateMaterials();
    }

    private void UpdateMaterials()
    {
        for (int i = 0; i < currentPalette.Length; i++)
        {
            if (Materials[i] != null)
            {
                Materials[i].color = currentPalette[i];
            }
        }
    }
}
