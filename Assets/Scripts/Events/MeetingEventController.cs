﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data;
using TMPro;
using UnityEngine;

namespace Events
{
    public class MeetingEventController : MonoBehaviour
    {
        [Header("Assets")]
        [SerializeField] private GameObject sentencePrefab;
        [SerializeField] private Transform parent;
        [SerializeField] private GameObject meetingArea;

        [Header("Accept/Decline")] 
        [SerializeField] private TextMeshProUGUI acceptText;
        [SerializeField] private TextMeshProUGUI declineText;
        
        private readonly List<Sentence> _conversation = new List<Sentence>();
        private Meeting _meetingInfo;
        private int _line = 0;
        private int sentence = 0;
        
        public static Action<string> StartMeeting;
        public static Action EndMeeting;

        private void OnEnable()
        {
            StartMeeting += BeginMeeting;
            //MAY replace later
            EndMeeting += FinishMeeting;
            
            meetingArea.SetActive(false);
        }

        private void OnDisable()
        {
            StartMeeting = null;
            EndMeeting = null;
        }

        private void Initialise(string meetingName)
        {
            GameData.CurrentMeeting = meetingName;
            _meetingInfo = SaveData.LoadConversation(GameData.CurrentMeeting);

            foreach (var line in _meetingInfo.MeetingConversation)
            {
                CreateSentence();
                CreateSentence();
            }

            MoveLineOn(_line, _meetingInfo.MeetingConversation[_line].Sentence);
        }

        private void CreateSentence()
        {
            var sentence = Instantiate(sentencePrefab, parent);
            var script = sentence.GetComponent<Sentence>();
            _conversation.Add(script);
        }
        
        public void Accept()
        {
            if (_conversation.Count == sentence+2)
            {
                sentence++;
                MoveLineOn(sentence, _meetingInfo.MeetingConversation[_line].Accept);
                EndMeeting?.Invoke();
            }
            else
            {
                sentence++;
                MoveLineOn(sentence, _meetingInfo.MeetingConversation[_line].Accept);
                sentence++;
                if (_meetingInfo.MeetingConversation.Count <= _line+1) return;
                _line++;
                MoveLineOn(sentence, _meetingInfo.MeetingConversation[_line].Sentence);
            }
        }

        public void Decline()
        {
            if (_conversation.Count == sentence+2)
            {
                sentence++;
                MoveLineOn(sentence, _meetingInfo.MeetingConversation[_line].Decline);
                EndMeeting?.Invoke();
            }
            else
            {
                sentence++;
                MoveLineOn(sentence, _meetingInfo.MeetingConversation[_line].Decline);
                sentence++;
                if (_meetingInfo.MeetingConversation.Count <= _line+1) return;
                _line++;
                MoveLineOn(sentence, _meetingInfo.MeetingConversation[_line].Sentence);
            }
            
        }

        private void MoveLineOn(int index, string sentence)
        {
            _conversation[index].sentence = sentence;
            _conversation[index].IntroAnimation();
            AssignAcceptDecline();
        }
        
        private void AssignAcceptDecline()
        {
            var currentConversation = _meetingInfo.MeetingConversation[_line];
            acceptText.text = currentConversation.Accept;
            declineText.text = currentConversation.Decline;
        }

        private void BeginMeeting(string meetingName)
        {
            Initialise(meetingName);
            meetingArea.SetActive(true);
        }

        private void FinishMeeting()
        {
            meetingArea.SetActive(false);
        }


    }
}
