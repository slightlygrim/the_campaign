﻿using DG.Tweening;
using TMPro;
using UnityEngine;

public class Sentence : MonoBehaviour
{
    [Header("Tween")]
    [SerializeField] private RectTransform sentenceTrans;
    [SerializeField] private float duration;
    
    [Header("Assets")] 
    [SerializeField] private TextMeshProUGUI sentenceTextMesh;

    public string sentence;
    
    public void IntroAnimation()
    {
        sentenceTextMesh.text = sentence;
        sentenceTrans.DOAnchorPos(Vector2.zero, duration);
    }
}
