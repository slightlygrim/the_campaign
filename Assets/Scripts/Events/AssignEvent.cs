﻿using System;
using Types;
using UI;
using UnityEngine;

public class AssignEvent : MonoBehaviour
{
    [SerializeField] private ScheduledEventType _event;

    public static Action<ScheduledEventType> ScheduleEvent;

    public void AssignEventCalendar()
    {
        WindowManager.CalendarWindow?.Invoke();
        CalendarController.NewEvent?.Invoke(_event);
    }

}
