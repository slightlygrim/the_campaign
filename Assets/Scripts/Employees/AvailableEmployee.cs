﻿using Data;
using Employees;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AvailableEmployee : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI infoMesh;
    [SerializeField] private Image employeeImage;
    
    private EmployeeData employeeInfo;
    private int assignedPosition;
    public EmployeeData EmployeeInfo
    {
        set => employeeInfo = value;
    }

    public void SetEmployeeInfo()
    {
        infoMesh.text = employeeInfo.Name;
        employeeImage.sprite = employeeInfo.employeeImage;
    }

    public void HireEmployee()
    {
        EmployeeController.AssignEmployee?.Invoke(employeeInfo);
    }
}
