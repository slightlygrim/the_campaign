﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Scheduling
{
    public class JobValue : MonoBehaviour
    {
        [Header("FundTween")] 
        [SerializeField] private TextMeshProUGUI textMesh;
        [SerializeField] private float duration;
        [SerializeField] private Color positive, negative;

        private RectTransform _textRect;
        private float _defaultY;
        private float minFunding = 1f, maxFunding = 10f;
    
        private void OnEnable()
        {
            _textRect = GetComponent<RectTransform>();
            _defaultY = _textRect.anchoredPosition.y;
            _textRect.anchoredPosition = Vector2.zero;
            var funding = Random.Range(minFunding, maxFunding);
            TextTween(positive, funding);
        }

        private void TextTween(Color colorValue, float funding)
        {
            _textRect.DOAnchorPos(new Vector2(0, _defaultY), duration).OnComplete(()=>
            {
                //FundingButton.ReduceCount?.Invoke();
                Destroy(gameObject);
            });
            textMesh.DOColor(new Color(colorValue.r, colorValue.g, colorValue.b, 255), 0.1f);
            textMesh.text = $"+${(int)funding}";
            GameData.Candidate.Funding += funding;
        }
    }
}
