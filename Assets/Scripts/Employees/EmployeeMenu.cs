﻿using UnityEngine;

public class EmployeeMenu : MonoBehaviour
{
   [SerializeField] private GameObject MenuButton;

   public void CreateSubMenu()
   {
      for (int i = 0; i < 3; i++)
      {
         Instantiate(MenuButton, transform);
      }
   }
   
}
