﻿using System;
using System.Collections.Generic;
using Data;
using Employees;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class EmployeeController : MonoBehaviour
{
    [Header("Windows")]
    [SerializeField] private GameObject AvailableEmployeeMenu;
    [SerializeField] private GameObject EmployeeDetailsMenu;
    
    [Header("Employees")]
    [SerializeField] private int numberEmployees;
    //[SerializeField] private GameObject employeePrefab;
    [SerializeField] private AssetReference employeeReference;
    [SerializeField] private Transform employeesParent;
    [SerializeField] private EmployeeDetails employeeDetails;
   
    
    public int currentEmployee;
    
    private List<EmployeeButton> employeeControllers = new List<EmployeeButton>();
    
    public static Action<int> TriggerAvailableMenu;
    public static Action TriggerEmployeeDetailMenu;
    public static Action<EmployeeData> AssignEmployee;

    private void Awake()
    {
        PopulateEmployees();
    }

    private void OnEnable()
    {
        TriggerAvailableMenu += SetAvailableMenu;
        TriggerEmployeeDetailMenu += SetEmployeeMenu;
        AssignEmployee += SetEmployee;
    }

    private void OnDisable()
    {
        TriggerAvailableMenu = null;
        TriggerEmployeeDetailMenu = null;
        AssignEmployee = null;
    }


    private void PopulateEmployees()
    {
        if(!employeeReference.RuntimeKeyIsValid())
        {
            Debug.Log("Invalid key" + employeeReference.RuntimeKey);
            return;
        }
        
        Addressables.LoadAssetAsync<GameObject>(employeeReference);
        Addressables.LoadAssetAsync<GameObject>(employeeReference).Completed += CreateEmployees;
    }

    private void CreateEmployees(AsyncOperationHandle<GameObject> employeePrefab)
    {
        for (var i = 0; i < numberEmployees; i++)
        {
            var employee = Addressables.InstantiateAsync(employeeReference, employeesParent).Result;
            var employeeScript = employee.GetComponent<EmployeeButton>();
            employeeScript.positionNumber = i;
            employeeScript.Controller = this;
            employeeControllers.Add(employeeScript);
        }
    }
    
/*    private void PopulateEmployees()
    {
        for (var i = 0; i < numberEmployees; i++)
        {
            var employee = Instantiate(employeePrefab, employeesParent);
            var employeeScript = employee.GetComponent<EmployeeButton>();
            employeeScript.positionNumber = i;
            employeeScript.Controller = this;
            employeeControllers.Add(employeeScript);
        }
    }*/
    
    private void SetEmployeeMenu()
    {
        employeeDetails.selectedEmployee = employeeControllers[currentEmployee].employeeData;
        EmployeeDetailsMenu.SetActive(true);
    }

    public void CloseEmployeeMenu()
    {
        EmployeeDetailsMenu.SetActive(false);
    }

    private void SetAvailableMenu(int assignmentNumber)
    {
        AvailableEmployeeMenu.SetActive(true);
    }

    private void CloseAvailableMenu()
    {
        AvailableEmployeeMenu.SetActive(false);
    }

    private void SetEmployee(EmployeeData employeeData)
    {
        employeeControllers[currentEmployee].SetEmployee(employeeData);
        CloseAvailableMenu();
    }

    private void DisplayEmployee()
    {
        SetEmployeeMenu();
    }
}
