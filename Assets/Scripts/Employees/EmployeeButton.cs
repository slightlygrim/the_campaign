﻿using System;
using Data;
using UnityEngine;
using UnityEngine.UI;

namespace Employees
{
    [RequireComponent(typeof(EmployeeProgress))]
    public class EmployeeButton : MonoBehaviour
    {
        [NonSerialized] public EmployeeData employeeData;
        [NonSerialized] public EmployeeController Controller;
        public int positionNumber;
        
        [Header("Image")] 
        [SerializeField] private Sprite plusIcon; 
        [SerializeField] private Sprite workerIcon;
        [SerializeField] private Image image;
        [SerializeField] private Button _button;

        
        [Header("Tasks")]
        [SerializeField] private GameObject task;
        [SerializeField] private TaskQueController taskQueController;

        private bool busy;
        private bool vacant = true;
        private EmployeeProgress _progress;

        private void OnEnable()
        {
            _button.onClick.AddListener(()=>
            {
                EmployeeController.TriggerAvailableMenu?.Invoke(positionNumber);
                Controller.currentEmployee = positionNumber;
            });
            
            _progress = GetComponent<EmployeeProgress>();
            _progress.progressComplete += CheckQue;
            ChangeIcon();
            task.SetActive(false);
        }

        private void Reset()
        {
            _button.interactable = true;
        }

        private void ChangeIcon()
        {
            image.sprite = vacant ? plusIcon : workerIcon;
            task.SetActive(true);
        }

        public void SetEmployee(EmployeeData employee)
        {
            employeeData = employee;
            vacant = false;
            ChangeIcon();
            task.SetActive(true);
            _button.onClick.RemoveAllListeners();
            _button.onClick.AddListener(() =>
            {
                Controller.currentEmployee = positionNumber;
                EmployeeController.TriggerEmployeeDetailMenu.Invoke();
            });
        }

        private void CheckQue()
        {
            taskQueController.taskQue.Remove(taskQueController.taskQue[0]);
            taskQueController.UpdateTaskQue();

            if (taskQueController.taskQue.Count > 0)
            {
                var task = taskQueController.taskQue[0];
                _progress.TweenBar(task.timeValue,task);
                busy = true;
            }
            else
            {
                busy = false;
            }
        }

        public void StartQue()
        {
            if (busy) return;
            var task = taskQueController.taskQue[0];
            _progress.TweenBar(task.timeValue, task);
            busy = true;
        }
    }
}
