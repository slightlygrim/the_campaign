﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Employees
{
    public class EmployeeProgress : MonoBehaviour
    {
        [Header("Components Needed")]
        [SerializeField] private RectTransform barRect;
        [SerializeField] private TaskValueController taskValueController;

        private TaskData currentTaskData;
        private float progressTime, previousTime;
        private bool taskStarted;
        
        public Action progressComplete;

        public void TweenBar(float time, TaskData task)
        {
            currentTaskData = task;
            progressTime = time;
            taskStarted = true;
        }

        private void Update()
        {
            if (taskStarted)
            {
                progressTime -= Time.deltaTime;

                var scale = barRect.localScale;
                scale.y = 1 / progressTime;
                barRect.localScale = scale;

                if (progressTime <= 0)
                {
                    taskStarted = false;
                    progressComplete?.Invoke();
                    ResetBar();
                }
                else
                {
                    previousTime += Time.deltaTime;

                    if (!(previousTime > 1f)) return;
                    if (Random.value > 0.5f)
                    {
                        taskValueController.TriggerValue(currentTaskData);
                    }

                    previousTime = 0;
                }
            }
        }

        private void ResetBar()
        {
            barRect.localScale = new Vector2(1, 0);
        }
    }
}
