﻿using System;
using Data;
using TMPro;
using UnityEngine;

public class EmployeeDetails : MonoBehaviour
{
    [NonSerialized] public EmployeeData selectedEmployee;

    [SerializeField] private TextMeshProUGUI detailsMesh;

    private void OnEnable()
    {
        DisplayEmployee();
    }

    private void DisplayEmployee()
    {
        var details = $"{selectedEmployee.title} {selectedEmployee.Name}";
        detailsMesh.text = details;
    }

    private void PopulateSkills()
    {
        
    }

    private void PopulateTasks()
    {
        
    }
}
