﻿using System;
using System.Collections.Generic;
using Employees;
using UnityEngine;
using UnityEngine.UI;

public class TaskQueController : MonoBehaviour
{
   [Header("Test")] [SerializeField] private List<Color> colorTest = new List<Color>();
   
   [Header("Prefabs")]
   [SerializeField] private GameObject taskPrefab;
   [SerializeField] private Transform parent;
   [SerializeField] private EmployeeButton employeeScript;
   
   [Header("Que")]
   [SerializeField] private int maxAvailableTasks;
   [SerializeField] private int maxQueTasks;
   [SerializeField] private List<Image> taskQueImage = new List<Image>();
   
   [NonSerialized] public readonly List<TaskData> taskQue = new List<TaskData>();
   private List<TaskButton> tasks = new List<TaskButton>();
   
   private void OnEnable()
   {
      PopulateTasks();
   }

   private void PopulateTasks()
   {
      for (var i = 0; i < maxAvailableTasks; i++)
      {
         var task = Instantiate(taskPrefab, parent);
         var taskScript = task.GetComponent<TaskButton>();
         taskScript.tempColor = colorTest[i];
         taskScript.taskQueController = this;
         tasks.Add(taskScript);
      }
   }

   public void AddTask(TaskData newTask)
   {
      if (taskQue.Count >= maxQueTasks) return;
      taskQue.Add(newTask);
      UpdateTaskQue();
      employeeScript.StartQue();
   }

   public void UpdateTaskQue()
   {
      for (var i = 0; i < taskQueImage.Count; i++)
      {
         taskQueImage[i].color = taskQue.Count > i ? taskQue[i].color : Color.white;
         //taskQueImage[i].texture = taskQue[i].taskImage;
      }
   }
}
