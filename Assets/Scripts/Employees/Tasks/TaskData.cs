﻿using System;
using System.Collections.Generic;
using Types;
using UnityEngine;

[Serializable]
public class TaskData
{
    public string title;
    public float votesValue;
    public float fundingValue;
    public float timeValue;
    public Texture2D taskImage;
    public Color color;
    public TaskValueEffected effectedValue;
    public int RequiredMediaPoints;
    public int RequiredPRPoints;
    public int RequiredWorkPoints;
}

[Serializable]
public class TasksData
{
    public string title;
    public List<TaskData> tasks = new List<TaskData>();
}
