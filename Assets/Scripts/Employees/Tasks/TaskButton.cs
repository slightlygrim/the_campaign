﻿using Types;
using UnityEngine;
using UnityEngine.UI;

public class TaskButton : MonoBehaviour
{

    public Color tempColor;
    public TaskData task;
    public TaskQueController taskQueController;
    
    private Button taskButton;
    
    private void OnEnable()
    {
        CreateRandomTaskTest();
        
        taskButton = GetComponent<Button>();
        taskButton.onClick.AddListener(() =>
        {
            taskQueController.AddTask(task);
        });
    }

    private void CreateRandomTaskTest()
    {
        int random = Random.Range(0, 3);

        if (random == 0)
        {
            task = CreateTask("Social", Color.magenta, 3f, TaskValueEffected.Funds);
        } else if (random == 1)
        {
            task = CreateTask("TV", Color.blue, 6f, TaskValueEffected.Votes);
        } else if (random == 2)
        {
            task = CreateTask("Public Relations", Color.green, 2f, TaskValueEffected.Funds);
        }
        else
        {
            task = CreateTask("Advert", Color.red, 4f, TaskValueEffected.Votes);
        }
    }

    private TaskData CreateTask( string title, Color color, float time, TaskValueEffected taskValueEffected)
    {
        return new TaskData {title = title, color = color, timeValue = time, fundingValue = Random.Range(-100,100), votesValue = Random.Range(-10,10), effectedValue = taskValueEffected};
    }
    
}
