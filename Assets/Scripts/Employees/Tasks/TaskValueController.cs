﻿using System;
using Types;
using UnityEngine;
using UnityEngine.UI;

public class TaskValueController : MonoBehaviour
{
    [SerializeField] private RectTransform voteRect;
    [SerializeField] private RectTransform fundingRect;
    [SerializeField] private Color positive;
    [SerializeField] private Color negative;
    
    private Image voteImage;
    private Image fundingImage;

    private void OnEnable()
    {
        voteImage = voteRect.GetComponent<Image>();
        fundingImage = fundingRect.GetComponent<Image>();
    }

    public void TriggerValue(TaskData value)
    {
        switch (value.effectedValue)
        {
            case TaskValueEffected.Funds:
                GameData.Candidate.Funding += value.fundingValue;
                ScaleRect(fundingRect, value.fundingValue);
                CheckColor(value.fundingValue, TaskValueEffected.Funds);
                break;
            case TaskValueEffected.Votes:
                GameData.Candidate.Votes += value.votesValue;
                ScaleRect(voteRect, value.votesValue);
                CheckColor(value.votesValue, TaskValueEffected.Votes);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void CheckColor(float value, TaskValueEffected valueEffected)
    {
        var image = CheckImage(valueEffected);
        image.color = value > 0 ? positive : negative;
    }

    private Image CheckImage(TaskValueEffected valueEffected)
    {
        return valueEffected == TaskValueEffected.Funds ? fundingImage : voteImage;
    }

    private void ScaleRect(RectTransform rect, float value)
    {
        var scale = rect.localScale;
        scale.y += value / 100; 
        scale.y = Mathf.Clamp(scale.y, -1, 1);
        rect.localScale = scale;
    }
 
    
    //[SerializeField] private GameObject taskPrefab;
    //private List<GameObject> TaskPool = new List<GameObject>();
    
    ///Pool encase decide to add values
    /*public void TriggerValue(TaskData taskData)
    {
        if (TaskPool.Count > 0)
        {
            foreach (var t in TaskPool)
            {
                if (!t.activeInHierarchy)
                {
                    CreateTaskValue(taskData);
                    return;
                }
            }

            CreateTaskValue(taskData);
        }
        else
        {
            CreateTaskValue(taskData);
        }
    }

    private void CreateTaskValue(TaskData taskData)
    {
        var taskObject = Instantiate(taskPrefab, transform);
        TaskPool.Add(taskObject);
        var taskScript = taskObject.GetComponent<TaskValues>();
        taskScript.TriggerValue(taskData);
    }*/
}
