﻿using System;
using Types;
using UnityEngine;

public class TaskValue : MonoBehaviour
{
    [SerializeField] private RectTransform valueRect;
    [SerializeField] private TaskValueEffected valueEffected;

    public void TriggerValue(TaskData taskData)
    {
        valueRect.localScale += new Vector3(0, CheckEffectedValue(taskData), 0);
    }

    private float CheckEffectedValue(TaskData value)
    {
        switch (valueEffected)
        {
            case TaskValueEffected.Funds:
                GameData.Candidate.Funding += value.fundingValue;
                return value.fundingValue;
            case TaskValueEffected.Votes:
                GameData.Candidate.Votes += value.votesValue;
                return value.votesValue;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
}
