﻿using UnityEngine;

public class EmployeesController : MonoBehaviour
{
    [Header("Prefabs")] 
    [SerializeField] private GameObject employeePrefab;
    [SerializeField] private RectTransform parent;
    
    //Temp
    private int numberEmployees;

    private void Awake()
    {
        for (var i = 0; i < numberEmployees; i++)
        {
            Instantiate(employeePrefab, parent);
        }
    }
}
