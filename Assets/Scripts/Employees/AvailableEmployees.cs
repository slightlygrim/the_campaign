﻿using Data;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.Serialization;

namespace Employees
{
    public class AvailableEmployees : MonoBehaviour
    {
        [SerializeField] private GameObject AvailableEmployeePrefab;
        [SerializeField] private AssetReference AvailableEmployeeReference;
        [SerializeField] private Transform parent;
        
        private EmployeesData AllAvailableEmployees;

        private void OnEnable()
        {
            AllAvailableEmployees = SaveData.LoadAllEmployeesData();
            //PopulateMenu();
            PopulateEmployees();
        }

        private void PopulateEmployees()
        {
            if(!AvailableEmployeeReference.RuntimeKeyIsValid())
            {
                Debug.Log("Invalid key" + AvailableEmployeeReference.RuntimeKey);
                return;
            }
        
            Addressables.LoadAssetAsync<GameObject>(AvailableEmployeeReference);
            Addressables.LoadAssetAsync<GameObject>(AvailableEmployeeReference).Completed += Populate;
        }

        private void Populate(AsyncOperationHandle<GameObject> employeePrefab)
        {
            foreach (var employee in AllAvailableEmployees.employees)
            {
                var button =  Addressables.InstantiateAsync(AvailableEmployeeReference, parent).Result;
                var buttonScript = button.GetComponent<AvailableEmployee>();
                buttonScript.EmployeeInfo = employee;
                buttonScript.SetEmployeeInfo();
            }
        }
        
        private void PopulateMenu()
        {
            if (AllAvailableEmployees.employees.Count <= 0)
            {
                Debug.Log("No available employees");
            }
            else
            {
                foreach (var employee in AllAvailableEmployees.employees)
                {
                    var button = Instantiate(AvailableEmployeePrefab, parent);
                    var buttonScript = button.GetComponent<AvailableEmployee>();
                    buttonScript.EmployeeInfo = employee;
                    buttonScript.SetEmployeeInfo();
                }
            }
           
        }
    }
}
