﻿using System;
using Funding;
using Scheduling;
using TMPro;
using UnityEngine;

namespace Calendar
{
    public class Time : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI timeText;

        private float currentTime;
        public static Action<float> UpdateTime;

        private void OnEnable()
        {
            UpdateTime += NewTime;
        }

        private void OnDisable()
        {
            UpdateTime = null;
        }

        private void Awake()
        {
            ResetTime();
            GameData.Hours = 8;
            GameData.DaysUsed = 0;
        }

        private void Update()
        {
            if (!(GameData.Hours < 1)) return;
            GameData.DaysUsed++;
            GameData.Hours = 8;
            ResetTime();
        }

        private void NewTime(float addedTime)
        {
            var point = addedTime % 1;
            switch (point)
            {
                case 0:
                    break;
                case 0.25f:
                    point = 15;
                    break;
                case 0.5f:
                    point = 30;
                    break;
                case 0.75f:
                    point = 45;
                    break;
            }

            currentTime += Mathf.Floor(addedTime);
            timeText.text = $"{currentTime:00}:{point:00}";
        }

        private void ResetTime()
        {
            currentTime = 8;
            timeText.text = $"{currentTime:00}:00";
        }
    }
}
