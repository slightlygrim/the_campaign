﻿using System;
using Events;
using TMPro;
using Types;
using UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace Calendar
{
   public class Date : MonoBehaviour
   {
      [SerializeField] private TextMeshProUGUI dateText;
      [SerializeField] private GameObject disabled;
      [SerializeField] private Button button;

      public int date;
      private ScheduledEventType _event;
      private float _triggerTime;
      
      private void Awake()
      {
         _event = ScheduledEventType.None;
         //disabled.SetActive(false);
      }

      public void SetDate(string day)
      {
         dateText.text = day;
      }
      
      public void DateDisabled()
      {
         if (disabled.activeSelf) return;
         button.interactable = false;
         //disabled.SetActive(true);
      }
   
      public void SetEvent(ScheduledEventType newEvent)
      {
         button.interactable = false;
         _event = newEvent;
         dateText.text = newEvent.ToString();
      }

      public void DateClicked()
      {
         CalendarController.AssignEvent?.Invoke(date - 1);
      }
   }
}
