﻿using System;
using System.Collections.Generic;
using Calendar;
using Scheduling;
using Types;
using UI;
using UnityEngine;
using UnityEngine.Serialization;

public class CalendarController : MonoBehaviour
{
    [FormerlySerializedAs("_day")] [SerializeField] private GameObject day;
    private readonly List<Date> _dates = new List<Date>();
    private ScheduledEventType _currentEvent, _assignEvent;
    
    public static Action<ScheduledEventType> NewEvent;
    public static Action<int> AssignEvent;

    private void OnEnable()
    {
        NewEvent += AssignScheduledEvent;
        AssignEvent += DateSelected;
        var month = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        if(_dates.Count < month)CreateMonth(month);
        DisableUsedDays();
    }

    private void OnDisable()
    {
        NewEvent = null;
    }

    private void CreateMonth(int month)
    {
        for (var i = 1; i <= month; i++)
        {
           var day = Instantiate(this.day, transform);
           day.name = i.ToString();
           var script = day.GetComponent<Date>();
           script.date = i;
           _dates.Add(script);
           script.SetDate(i.ToString("00"));
        }
    }
    
    private void DisableUsedDays()
    {
        for (var i = 0; i < GameData.DaysUsed; i++)
        {
            DisableDate(i);
        }
    }
    
    private void DisableDate(int day)
    {
        _dates[day].DateDisabled();
    }

    private void AssignScheduledEvent(ScheduledEventType newEvent)
    {
        _assignEvent = newEvent;
    }

    public void DateSelected(int day)
    {
        if(_assignEvent == ScheduledEventType.None) return;
        _dates[day].SetEvent(_assignEvent);
        WindowManager.ScheduleWindow?.Invoke();
        _assignEvent = ScheduledEventType.None;
    }
}
