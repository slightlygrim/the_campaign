﻿using System;
using System.Collections.Generic;
using Data;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Testing
{
    public class LoadTestData : MonoBehaviour
    {
        private List<CandidateData> _allCandidates;
        
        private void Awake()
        {
            if (GameData.Candidate != null) return;
            _allCandidates = SaveData.LoadCandidatesData().candidates;
            GameData.Candidate = _allCandidates[0];
            SelectOpponents();
        }
        
        private void SelectOpponents()
        {
            var opponentCount = Random.Range(3, 6);
            for (var i = 0; i < opponentCount; i++)
            {
                var opponent = _allCandidates[Random.Range(0, _allCandidates.Count)];
                if(opponent.Name == GameData.Candidate.Name) return;
                GameData.Opponents.Add(opponent);
            }
        }
    }
}
