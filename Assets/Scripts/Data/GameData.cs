﻿using System.Collections.Generic;
using Data;

public static class GameData
{
    //Candidate
    public static CandidateData Candidate { get; set; }

    public static List<CandidateData> Opponents { get; set; } = new List<CandidateData>();

    
    //Votes
    public static int maxVotes { get; set; }


    //Dates and times
    public static float Hours { get; set; }

    public static int DaysUsed { get; set; }
    
    public static int EndDate { get; set; }

    
    //Event Settings
    public static string CurrentMeeting { get; set; }

}
