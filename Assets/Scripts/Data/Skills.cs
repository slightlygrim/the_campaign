﻿using System;
using System.Collections.Generic;
using Types;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu(order = 0, menuName = "skills", fileName = "skillList")]
public class Skills : ScriptableObject
{ 
    public List<SkillsData> skills = new List<SkillsData>();
}

[Serializable]
public class SkillsData
{
    [FormerlySerializedAs("skillEnum")] public EmployeeTypeSkills typeSkillEnum;
    public List<SkillsPrefabs> prefabs = new List<SkillsPrefabs>();

}

[Serializable]
public class SkillsPrefabs
{
    public int level;
    public List<GameObject> prefabs = new List<GameObject>();
}
