﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace Data
{
    public static class SaveData
    {
        public static void SaveCandidates(CandidatesData data)
        {
            //Write some text to the test.txt file
            var writer =  new StreamWriter(Application.persistentDataPath + "/Candidates/CandidateData.json");
            var json = JsonUtility.ToJson(data.candidates);
            writer.Write(json);
            writer.Close();
        }

        public static void SaveMeetings(Meetings data)
        {
            var writer =  new StreamWriter(Application.persistentDataPath + "/Events/Meetings.json");
            var json = JsonUtility.ToJson(data);
            writer.Write(json);
            writer.Close();
        }

        public static CandidatesData LoadCandidatesData()
        {
            //Read the text from directly from the test.txt file
            var reader = new StreamReader(Application.persistentDataPath + "/CandidateData.json");
            var json = reader.ReadToEnd();
            var candidatesData = JsonUtility.FromJson<CandidatesData>(json);
            reader.Close();
            return candidatesData;
        }

        public static EmployeesData LoadAllEmployeesData()
        {
            var reader = new StreamReader(Application.persistentDataPath + "/Employees/EmployeesData.json");
            var json = reader.ReadToEnd();
            var employeesData = JsonUtility.FromJson<EmployeesData>(json);
            reader.Close();
            return employeesData;
        }
            
        public static Meetings LoadMeetings()
        {
            if (!File.Exists(Application.persistentDataPath + "/Events/Meetings.json")) return null;
            var reader = new StreamReader(Application.persistentDataPath + "/Events/Meetings.json");
            var json = reader.ReadToEnd();
            var meetingsData = JsonUtility.FromJson<Meetings>(json);
            reader.Close();
            return meetingsData;
        }

        public static Meeting LoadConversation(string title)
        {
            var events = SaveData.LoadMeetings();
            return events.MeetingsList.FirstOrDefault(meeting => meeting.title == title);
        }

        public static List<TasksData> LoadAllTasksData()
        {
            if (!Directory.Exists(Application.persistentDataPath + "/Tasks")) return null;
            var path = Application.persistentDataPath + "/Tasks";
            var info = new DirectoryInfo(path);
            var fileInfo = info.GetFiles();
            var tasksData = new List<TasksData>();
            
            foreach (var file in fileInfo)
            {
                var reader = new StreamReader(file.OpenRead());
                var json = reader.ReadToEnd();
                var taskData = JsonUtility.FromJson<TasksData>(json);
                tasksData.Add(taskData);
                reader.Close();
            }
            
            return tasksData;
        }

        public static void SaveAllTasksData(List<TasksData> AllTasks)
        {
            foreach (var task in AllTasks)
            {
                var writer =  new StreamWriter(Application.persistentDataPath + "/Events" + task.title + ".json");
                var json = JsonUtility.ToJson(task);
                writer.Write(json);
                writer.Close();
            }
        }

        public static TasksData LoadSpecificTasks(string specificFile)
        {
            var reader = new StreamReader(Application.persistentDataPath + "/Tasks/" + specificFile + ".json");
            var json = reader.ReadToEnd();
            var tasksData = JsonUtility.FromJson<TasksData>(json);
            reader.Close();
            return tasksData;
        }

        public static void SaveSpecificTasks(TasksData tasks)
        {
            var writer =  new StreamWriter(Application.persistentDataPath + "/Events" + tasks.title + ".json");
            var json = JsonUtility.ToJson(tasks);
            writer.Write(json);
            writer.Close();
        }
    }
}
