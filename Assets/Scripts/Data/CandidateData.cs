﻿using System;
using Sirenix.OdinInspector;
using Types;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class CandidateData
    {
        [TabGroup("Candidate", "Personal Details")]
        [HorizontalGroup("Candidate/Personal Details/Left", Width = 50), HideLabel, PreviewField(50)]
        public Texture2D thumbnail;
        
        [TabGroup("Candidate", "Personal Details")]
        [VerticalGroup("Candidate/Personal Details/Left/Right")]
        public TitleType Title;
        
        [TabGroup("Candidate", "Personal Details"), VerticalGroup("Candidate/Personal Details/Left/Right")]
        public string Name;
        
        [TabGroup("Candidate", "Personal Details"), VerticalGroup("Candidate/Personal Details/Left/Right")]
        public int Age;
        
        [TabGroup("Candidate", "Political Details")] public DistrictType District;
        [TabGroup("Candidate", "Political Details")]public PartyType Party;
        [TabGroup("Candidate", "Political Details")]public Color color;
        
        [TabGroup("Candidate", "Game Details")] public float StartingVotes;
        [TabGroup("Candidate", "Game Details")] public float Votes;
        [TabGroup("Candidate", "Game Details")] public float StartingFunding;
        [TabGroup("Candidate", "Game Details")] public float Funding;
    }
}