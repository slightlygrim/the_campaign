﻿using System;
using System.Collections.Generic;

namespace Data
{
    [Serializable]
    public class EmployeesData
    {
        public List<EmployeeData> employees = new List<EmployeeData>();
    }
}
