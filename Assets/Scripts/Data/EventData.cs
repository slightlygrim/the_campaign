﻿using System;
using System.Collections.Generic;


namespace Data
{
    [Serializable]
    public class Meetings
    {
        public List<Meeting> MeetingsList = new List<Meeting>();
    }

    [Serializable]
    public class Meeting
    {
        public List<MeetingData> MeetingConversation = new List<MeetingData>();
        public string title;
    }
    
    [Serializable]
    public class MeetingData
    {
        public string Sentence;
        public string Accept;
        public string Decline;
        public float AcceptValue;
        public float DeclineValue;
    }
}
