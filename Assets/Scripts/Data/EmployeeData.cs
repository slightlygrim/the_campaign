﻿using System;
using System.Collections.Generic;
using Types;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class EmployeeData
    {
        public TitleType title;
        public string Name;
        public string jobTitle;
        public int numberSkills;
        public Sprite employeeImage;
        public EmployeeTypeSkills TypeSkills;
    }
}
