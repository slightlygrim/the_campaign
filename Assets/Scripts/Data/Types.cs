﻿namespace Types
{
    public enum PartyType
    {
        None,
        Democrat,
        Republican
    }

    public enum DistrictType
    {
        North,
        South,
        East,
        West
    }

    public enum TitleType
    {
        None,
        Mr, 
        Mrs,
        Miss,
        Doc,
        Prof
    }

    public enum ScheduledEventType
    {
        None,
        LobbyMeeting,
        PressConference,
        BreakingNews
    }

    public enum EmployeeTypeSkills
    {
        None,
        Meetings,
        SocialMedia
    }
    
    public enum TaskValueEffected
    {
        Votes,
        Funds
    }

    public enum CardType
    {
        None,
        Task,
        Event
    }
}
