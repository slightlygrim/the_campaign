﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    [Serializable]
    public class CandidatesData
    {
        public List<CandidateData> candidates = new List<CandidateData>();
    }
}
