﻿using Data;
using Types;
using UnityEngine;

public class DefaultData : MonoBehaviour
{
    private float votes;
    
    private void Awake()
    {
        if (GameData.Candidate != null) return;
        GameData.Candidate = CreateCandidate();
        GameData.maxVotes = 1260;
        votes = 1260;

        for (var i = 0; i < 3; i++)
        {
            GameData.Opponents.Add(CreateOpponent());
        }

    }

    private static CandidateData CreateCandidate()
    {
        var candidate = new CandidateData
        {
            Name = "Linda Smith",
            Age = 45,
            District = DistrictType.East,
            StartingFunding = 2000,
            StartingVotes = 200,
            Party = PartyType.Democrat,
            color = Color.blue
        };

        return candidate;
    }

    private CandidateData CreateOpponent()
    {
        var candidate = new CandidateData
        {
            Name = "John Smith",
            Age = Random.Range(34, 64),
            District = DistrictType.East,
            StartingFunding = Random.Range(2,100),
            StartingVotes = Random.Range(200, votes),
            Party = PartyType.Democrat,
            color = Color.blue
        };

        votes -= candidate.StartingVotes;
        return candidate;
    }
}
