﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UI
{
    public class WindowManager : MonoBehaviour
    {
        [SerializeField]private List<GameObject> windows = new List<GameObject>();

        private GameObject _previousWindow;

        public static Action CalendarWindow; 
        public static Action ScheduleWindow; 
        
        private void OnEnable()
        {
            CalendarWindow += ActivateCalendarWindow;
            ScheduleWindow += ActivateScheduleWindow;
            GetWindows();
        }

        private void OnDisable()
        {
            CalendarWindow = null;
        }

        private void GetWindows()
        {
            foreach (Transform child in transform)
            {
                var currentObject = child.gameObject;
                if (child.gameObject.name == "Tabs") continue;
                currentObject.SetActive(false);
                windows.Add(currentObject);
            }
            
            windows[0].SetActive(true);
            _previousWindow = windows[0];
        }
        
        public void ChangeWindow(string window)
        {
            var currentWindow = FindWindow(window);
            if (currentWindow == null) return;
            currentWindow.SetActive(true);
            _previousWindow.SetActive(false);
            _previousWindow = currentWindow;
        }

        private GameObject FindWindow(string windowName)
        {
            return windows.FirstOrDefault(window => window.name == windowName);
        }

        private void ActivateCalendarWindow()
        {
          ChangeWindow("CalendarMenu");  
        }
        
        private void ActivateScheduleWindow()
        {
            ChangeWindow("SchedulingMenu");  
        }
    }
}
