﻿using Data;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectMenuControls : MonoBehaviour
{
    [Header("Stats")]
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI districtText;
    [SerializeField] private TextMeshProUGUI popularityText;
    //[SerializeField] private RawImage thumbnailImage;

    private CandidatesData _candidatesData;
    private CandidateData _currentCandidate;
    private int _currentCandidateValue;
    
    private void OnEnable()
    {
        _candidatesData = SaveData.LoadCandidatesData();
        _currentCandidate = _candidatesData.candidates[0];
        _currentCandidateValue = 0;
        LoadTextData();
    }

    public void NextCandidate()
    {
        if (_currentCandidateValue + 1 >= _candidatesData.candidates.Count)
        {
            _currentCandidateValue = 0;
        }
        else
        {
            _currentCandidateValue++;
        }
        
        _currentCandidate = _candidatesData.candidates[_currentCandidateValue];
        LoadTextData();
    }
    
    public void PreviousCandidate()
    {
        if (_currentCandidateValue -1 < 0)
        {
            _currentCandidateValue = _candidatesData.candidates.Count-1;
        }
        else
        {
            _currentCandidateValue--;
        }
        
        _currentCandidate = _candidatesData.candidates[_currentCandidateValue];
        LoadTextData();
    }

    private void LoadTextData()
    {
        nameText.text = $"{_currentCandidate.Title} {_currentCandidate.Name}";
        districtText.text = _currentCandidate.District.ToString();
        popularityText.text = _currentCandidate.Votes.ToString("00");
        //thumbnailImage.texture = _currentCandidate.thumbnail;
    }

    public void SelectedCandidate()
    {
        GameData.Candidate = _currentCandidate;
    }
    
    public void SelectOpponents()
    {
        var opponentCount = Random.Range(3, 6);
        for (var i = 0; i < opponentCount; i++)
        {
            var opponent = _candidatesData.candidates[Random.Range(0, _candidatesData.candidates.Count)];
            if(opponent.Name == GameData.Candidate.Name) return;
            GameData.Opponents.Add(opponent);
        }
    }

    public void LoadGame()
    {
        SceneManager.LoadScene("MainGame");
    }
}
