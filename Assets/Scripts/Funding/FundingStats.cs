﻿using System;
using TMPro;
using UnityEngine;

namespace Funding
{
    public class FundingStats : MonoBehaviour
    {
        [SerializeField]private TextMeshProUGUI totalText;

        public static Action UpdateStats;

        private void OnEnable()
        {
            UpdateStats += UpdateTotal;
            UpdateTotal();
        }

        private void OnDisable()
        {
            UpdateStats = null;
        }

        private void UpdateTotal()
        {
            totalText.text = $"${GameData.Candidate.Funding:F2}";
        }
    }
}
