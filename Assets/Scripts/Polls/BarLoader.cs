﻿using System.Collections.Generic;
using Data;
using TMPro;
using UnityEngine;

namespace Polls
{
    public class BarLoader : MonoBehaviour
    {
        [Header("Bar Graph")]
        [SerializeField] private GameObject prefabBar;
        [SerializeField] private Transform parent;

        [Header("Units")] 
        [SerializeField] private TextMeshProUGUI TopUnit, middleUnit;

        private void Awake()
        {
            CreateBar(GameData.Candidate.Votes, GameData.Candidate);
            
            foreach (var opponent in GameData.Opponents)
            {
                CreateBar(opponent.Votes, opponent);
            }
            
            SetUnits();
        }

        private void CreateBar(float votes, CandidateData candidate)
        {
            var bar = Instantiate(prefabBar, parent);
            var controls = bar.GetComponent<BarControls>();
            controls.SetData(candidate, votes/GameData.maxVotes);
        }

        private void SetUnits()
        {
            var votes = GameData.maxVotes;
            TopUnit.text = $"{votes}K";
            middleUnit.text = $"{votes / 2}K";
        }
    }
}
