﻿using Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Polls
{
    public class BarControls : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private RectTransform _rect;
        private Image barImage;

        private void Awake()
        {
            barImage = _rect.GetComponent<Image>();
        }

        public void SetData(CandidateData candidate, float votes)
        {
            titleText.text = candidate.Name;
            _rect.localScale = new Vector3(1, votes, 1);
            if(barImage != null)barImage.color = candidate.color;
        }
    }
}
