﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards
{
    [RequireComponent(typeof(CanvasGroup))]
    public class DragDrop : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler
    {
        public Types.CardType cardType;
        public Transform previousParent;
        [SerializeField] private Canvas canvas;
        
        private int previousChildIndex;
        private RectTransform rectTrans;
        private CanvasGroup canvasGroup;
        
        private void Awake()
        {
            rectTrans = GetComponent<RectTransform>();
            canvasGroup = GetComponent<CanvasGroup>();
            previousParent = transform.parent;
            previousChildIndex = transform.GetSiblingIndex();
            canvas = transform.parent.parent.GetComponent<Canvas>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            canvasGroup.alpha = 0.6f;
            canvasGroup.blocksRaycasts = false;
        }

        public void OnDrag(PointerEventData eventData)
        {
            rectTrans.anchoredPosition += eventData.delta/ canvas.scaleFactor;
            transform.parent = canvas.transform;
        }
    
        public void OnEndDrag(PointerEventData eventData)
        {
            transform.parent = previousParent;
            transform.SetSiblingIndex(previousChildIndex);
            canvasGroup.alpha = 1f;
            canvasGroup.blocksRaycasts =true;
        }

        public void ResetPosition()
        {
            transform.parent = previousParent;
            canvasGroup.alpha = 1f;
            canvasGroup.blocksRaycasts =true;
        }
    }
}
