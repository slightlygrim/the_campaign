﻿using System.Collections.Generic;
using Cards;
using UnityEngine;
using UnityEngine.UI;

public class TaskCardHolderQue : CardHolder
{
    [SerializeField] private List<RawImage> taskImage = new List<RawImage>();
    private List<TaskData> TaskQue = new List<TaskData>();

    private void OnEnable()
    {
        CardPlaced += AddTask;
    }

    private void AddTask(TaskData task)
    {
        if (TaskQue.Count < taskImage.Count)
        {
            TaskQue.Add(task);
            UpdateTasks();
        }
    }

    private void UpdateTasks()
    {
        for (var i = 0; i < taskImage.Count; i++)
        {
            if (i < TaskQue.Count)
            {
                var data = TaskQue[i];
                if(data != null)taskImage[i].color = data.color;
                //taskImage[i].texture = TaskQue[i].taskImage;
            }
            else
            {
                taskImage[i].color = Color.white;
            }
        }
    }
    
}
