﻿using System;
using Types;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Cards
{
    public class CardHolder : MonoBehaviour, IDropHandler
    {
        [Header("Card Holder")]
        [SerializeField] private CardType cardType;

        public Action<TaskData> CardPlaced;

        private void OnDisable()
        {
            CardPlaced = null;
        }

        public void OnDrop(PointerEventData eventData)
        {
            if (eventData.pointerDrag != null && eventData.pointerDrag.GetComponent<CardData>().cardType == cardType)
            {
                var cardDrag = eventData.pointerDrag.GetComponent<CardData>();
                CardPlaced?.Invoke(cardDrag.task);
                cardDrag.ResetCard();
            }
        }
    }
}
