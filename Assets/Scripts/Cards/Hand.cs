﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Random = UnityEngine.Random;

public class Hand : MonoBehaviour
{
    [SerializeField]private List<TaskData> AllTask = new List<TaskData>();
    [SerializeField] private List<GameObject> cardPool = new List<GameObject>();
    [SerializeField] private AssetReference cardPrefab;
    [SerializeField] private Transform cardPosition;
    [SerializeField] private int maxHand;

    private int activeCards;

    public static Action ReduceActiveCards;
    
    private void OnEnable()
    {
        ReduceActiveCards += ()=> activeCards--;
        
        if(!cardPrefab.RuntimeKeyIsValid())
        {
            Debug.Log("Invalid key" + cardPrefab.RuntimeKey);
            return;
        }
        
        Addressables.LoadAssetAsync<GameObject>(cardPrefab);
        Addressables.LoadAssetAsync<GameObject>(cardPrefab).Completed += SetPool;
    }

    private void OnDisable()
    {
        ReduceActiveCards = null;
    }

    private void SetPool(AsyncOperationHandle<GameObject> cardPrefab)
    {
        for (var i = 0; i < maxHand; i++)
        {
            CreateCard();
            activeCards = 0;
        }
    }

    public void DrawCards()
    {
        var numberCards = maxHand - activeCards;
        
        for (var i = 0; i < numberCards; i++)
        {
            if (!CheckCards())
            {
                CreateCard();
            }
        }
    }

    private bool CheckCards()
    {
        foreach (var card in cardPool.Where(card => !card.activeSelf))
        {
            card.GetComponent<CardData>().SetCard(AllTask[Random.Range(0, AllTask.Count)]);
            card.SetActive(true);
            activeCards++;
            return true;
        }

        return false;
    }
    
    private void CreateCard()
    {
        var card = Addressables.InstantiateAsync(cardPrefab, cardPosition).Result;
        cardPool.Add(card);
        card.GetComponent<CardData>().SetCard(AllTask[Random.Range(0, AllTask.Count)]);
        card.SetActive(false);
    }
}
