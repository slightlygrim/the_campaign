﻿using Cards;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardData : DragDrop
{
    public TaskData task;
    [SerializeField] private RawImage CardImage;
    [SerializeField] private TextMeshProUGUI title;

    private void OnEnable()
    {
        SetCard(task);
    }

    public void SetCard(TaskData task)
    {
        this.task = task;
        //title.text = task.title;
        CardImage.color = task.color;
    }
    
    
    public void ResetCard()
    {
        task = null;
        ResetPosition();
        Hand.ReduceActiveCards?.Invoke();
        gameObject.SetActive(false);
    }
}
